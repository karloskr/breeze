<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Colombia */
        City::create( [
            'id'=>1,
            'state_id'=>1,
            'name'=>'Leticia'
            ] );
        City::create( [
            'id'=>2,
            'state_id'=>2,
            'name'=>'Medellín'
            ] );
        City::create( [
            'id'=>3,
            'state_id'=>3,
            'name'=>'Bogota'
            ] );
        City::create( [
            'id'=>4,
            'state_id'=>4,
            'name'=>'Tunja'
            ] );
        City::create( [
            'id'=>5,
            'state_id'=>5,
            'name'=>'Pasto'
            ] );

        /* Estados Unidos */
        City::create( [
            'id'=>6,
            'state_id'=>6,
            'name'=>'Boston'
            ] );
        City::create( [
            'id'=>7,
            'state_id'=>7,
            'name'=>'Carson City'
            ] );
        City::create( [
            'id'=>8,
            'state_id'=>8,
            'name'=>'Denver'
            ] );
        City::create( [
            'id'=>9,
            'state_id'=>9,
            'name'=>'Atlanta'
            ] );
    }
}
