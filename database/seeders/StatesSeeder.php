<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Colombia */
        State::create( [
		'id'=>1,
		'country_id'=>1,
		'name'=>'Amazonas'
		] );

        State::create( [
		'id'=>2,
		'country_id'=>1,
		'name'=>'Antioquia'
		] );

        State::create( [
		'id'=>3,
		'country_id'=>1,
		'name'=>'Distrito Capital'
		] );

        State::create( [
		'id'=>4,
		'country_id'=>1,
		'name'=>'Boyacá'
		] );

        State::create( [
		'id'=>5,
		'country_id'=>1,
		'name'=>'Nariño'
		] );

        /* Estados Unidos */
		State::create( [
		'id'=>6,
		'country_id'=>2,
		'name'=>'Massachusetts'
		] );	
        
        State::create( [
		'id'=>7,
		'country_id'=>2,
		'name'=>'Nevada'
		] );

        State::create( [
		'id'=>8,
		'country_id'=>2,
		'name'=>'Colorado'
		] );

        State::create( [
		'id'=>9,
		'country_id'=>2,
		'name'=>'Georgia'
		] );
		
    }
}
