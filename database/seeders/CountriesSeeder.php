<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create( [
            'id'=>1,
            'name'=>'Colombia'
            ] );

        Country::create( [
            'id'=>2,
            'name'=>'Estados Unidos'
            ] );
    }
}
