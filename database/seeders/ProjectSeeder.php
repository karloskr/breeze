<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project = [
            ['title' => 'Mi primer Proyecto','url' => 'mi-primer-proyecto','description' => 'Descripcion de primer Proyecto'],
            ['title' => 'Mi segundo Proyecto', 'url' => 'mi-segundo-proyecto','description' => 'Descripcion de segundo Proyecto'],
            ['title' => 'Mi tercer Proyecto','url' => 'mi-tercer-proyecto','description' => 'Descripcion de tercer Proyecto'],
            ['title' => 'Mi cuarto Proyecto','url' => 'mi-cuarto-proyecto','description' => 'Descripcion de cuarto Proyecto'],
        ];

        foreach ($project as $pro) {
            Project::create($pro);
        }
    }
}
