@if (session('status'))
    <div class="col-12 alert alert-success alert-dismissible fade show" role="alert">
        {{ session('status') }}
  </div>
@endif