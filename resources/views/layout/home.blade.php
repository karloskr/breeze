<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="{{ asset('fontawesome-free/css/all.min.css') }}">

    @yield('css')

</head>
<body>
    <main>
        @include('layout.navbar')
        <br>
        <div class="container">
            @auth
                <span class="float-right">Session Start : {{auth()->user()->name}}</span>
            @endauth
        </div>
        @yield('content')
    </main>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('jquery-easing/jquery.easing.js')}}"></script>
    <script src="{{ asset('jquery-easing/jquery.easing.min.js')}}"></script>

    <script src="{{ asset('jquery-ui/jquery-ui.js')}}"></script>
    <script src="{{ asset('jquery-ui/jquery-ui.min.js')}}"></script>
    @yield('scripts')
</body>
</html>