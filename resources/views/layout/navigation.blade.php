<header>
    <div class="container">
        <nav class="navbar float-right">           
            <div>
                <a href="{{ route('login') }}" class="btn btn-outline-primary">Log In</a>
                <a href="{{ route('register') }}" class="btn btn-outline-danger">Register</a>
            </div>
          </nav>
    </div>
</header>