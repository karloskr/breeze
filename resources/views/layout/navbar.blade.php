<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              @auth
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link {{ setActive('users.index') }}" href="{{ route('users.index') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ setActive('about') }}" href="{{ route('about') }}">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ setActive('projects.*') }} " href="{{ route('projects.index') }}">Portafolio</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ setActive('contact') }}" href="{{ route('contact') }}">Contact</a>
                </li>
              </ul>
              @endauth
            </div>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search" autocomplete="off">
              <button class="btn btn-outline-success my-2 my-sm-0 mr-4" type="submit">Search</button>
          </form>

              @guest
                <a class="btn btn-outline-primary mr-2 {{ setActive('login') }}" href="{{ route('login') }}">Login</a>
                <a class="btn btn-outline-warning mr-2 {{ setActive('register') }}" href="{{ route('register') }}">Register</a>
              @else
              <li class="nav-item dropdown" style="list-style:none">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{auth()->user()->name}}
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();" >
                        {{ __('Log Out') }}
                    </a>
                  </form>
                </div>
              </li>
              {{-- <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a href="route('logout')"
                        onclick="event.preventDefault();
                                    this.closest('form').submit();" class="btn btn-outline-danger">
                    {{ __('Log Out') }}
                </a>
              </form> --}}

              @endguest
              
          </nav>
    </div>
</header>