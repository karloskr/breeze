@extends('layout.home')

@section('title, Edit Project')
    
@section('content')
    
    <div class="container">
        <h1>Editar Nuevo Proyecto</h1>

        <form method="POST" action="{{ route('projects.update',$project) }}">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="title">Titulo del Proyecto</label>
              <input type="text" class="form-control" name="title" value="{{ old('title', $project->title) }}">
            </div>
            @error('title')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
            <div class="form-group">
                <label for="url">Url del Proyecto</label>
                <input type="text" class="form-control" name="url" value="{{ old('url', $project->url) }}">
            </div>
            @error('url')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
            <div class="form-group">
                <label for="description">Descripcion del Proyecto</label>
                <textarea class="form-control" name="description" rows="3">{{ old('description', $project->description) }}</textarea>
            </div>
            @error('description')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
            <button type="submit" class="btn btn-primary">Actualizar</button>
          </form>
    </div>

@endsection
@endsection