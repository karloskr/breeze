@extends('layout.home')

@section('title, Create Project')
    
@section('content')
    
    <div class="container">
        <h1>Crear Nuevo Proyecto</h1>
        <form method="POST" action="{{ route('projects.store') }}">
            @csrf
            <div class="form-group">
                <label for="title">Titulo del Proyecto</label>
                <input type="text" class="form-control" name="title">
              </div>
              @error('title')
                      <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
              <div class="form-group">
                  <label for="url">Url del Proyecto</label>
                  <input type="text" class="form-control" name="url">
              </div>
              @error('url')
                      <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
              <div class="form-group">
                  <label for="description">Descripcion del Proyecto</label>
                  <textarea class="form-control" name="description" rows="3"></textarea>
              </div>
              @error('description')
                      <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
              <button type="submit" class="btn btn-primary">Guardar</button>
          </form>
    </div>

@endsection
@endsection