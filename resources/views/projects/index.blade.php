@extends('layout.home')

@section('title, Portafolio')
    
@section('content')
    
    <div class="container">
        @include('session-status.message')
        <h1>Portafolio</h1>
        <hr>
        @auth
        <div class="float-left mb-2">
            <a href="{{ route('projects.create') }}" class="btn btn-primary">Crear</a>
        </div>
        @endauth

        {{-- Table --}}
        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col"></th>
                <th scope="col"></th>
                
              </tr>
            </thead>
            <tbody>
                @forelse ($projects as $project)
                <tr>
                <th scope="row">{{ $project->id }}</th>
                <td><a href="{{ route('projects.show', $project) }}">{{ $project->title }}</a></td>
                @auth
                <td><a href="{{ route('projects.edit',$project) }}" class="btn btn-success">Edit</a></td>
                <td>
                    <form action="{{ route('projects.destroy',$project) }}" method="POST">
                        @csrf @method('DELETE')
                        <button class="btn btn-danger" >Delete</button>
                    </form>
                </td>
                @endauth
                @empty
                    <th>Null</th>
                    <td>No hay proyectos para mostrar</td>
                </tr>
                @endforelse
            </tbody>
          </table>

          {{ $projects->links() }}
    </div>

@endsection
@endsection