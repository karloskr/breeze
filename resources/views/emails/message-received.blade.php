<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mensaje Recibido</title>
</head>
<body>
    <div class="container">
        <p>Recibiste un email de : {{ $msg['name'] }} - {{ $msg['email'] }}</p>
        <p><strong>Asunto: {{ $msg['asunto'] }}</strong></p>
        <p><strong>Contenido: {{ $msg['content'] }}</strong></p>
    </div>
</body>
</html>