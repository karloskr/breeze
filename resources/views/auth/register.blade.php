@extends('layout.home')

@section('content')
@inject('countries', 'App\Services\Countries')
<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        {{-- <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" /> --}}

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" autofocus />
                {{-- Error Validation --}}
                @error('name')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"  {{-- pattern="\w+.\w+.\w+@\w+\.com" --}} />
                {{-- Error Validation --}}
                @error('email')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                             autocomplete="new-password" />
                {{-- Error Validation --}}
                @error('password')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" />
            </div>

            <!-- Cellphone -->
            <div class="mt-4">
                <x-label for="cell" :value="__('Celular')" />

                <x-input id="cell" class="block mt-1 w-full" type="number" name="cell" :value="old('cell')" autofocus />
                {{-- Error Validation --}}
                @error('cell')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror
            </div>

            <!-- Cedula -->
            <div class="mt-4">
                <x-label for="cedula" :value="__('Cedula')" />

                <x-input id="cedula" class="block mt-1 w-full" type="number" name="cedula" :value="old('cedula')" />
                {{-- Error Validation --}}
                @error('cedula')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror
            </div>

            <!-- Fecha de Nacimiento -->
            <div class="mt-4">
                <x-label for="nacimiento" :value="__('Fecha de Nacimiento')" />

                <x-input id="nacimiento" class="block mt-1 w-full" type="date" name="nacimiento" :value="old('nacimiento')" />
                @error('nacimiento')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror
            </div>

            <!-- Paises -->
            <div class="mt-4">
                <x-label for="country" :value="__('Paises')" />
                <select class="form-control" id="country" name="country_id">
                    @foreach($countries->get() as $index => $country)
                        <option value="{{ $index }}" {{ old('country_id') == $index ? 'selected' : '' }}>
                            {{ $country }}
                        </option>
                    @endforeach
                </select>
            </div>

            <!-- Estados -->
            <div class="mt-4">
                <x-label for="state" :value="__('Estados')" />
                <select class="form-control" id="state" name="state_id" data-old="{{ old('state_id') }}"></select>
            </div>

            <!-- Ciudades -->
            <div class="mt-4">
                <x-label for="city" :value="__('Ciudades')" />
                <select class="form-control" id="city" name="city_id" data-old="{{ old('city_id') }}"></select>
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
@endsection

@section('css')
<style>
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }
    input[type=number] { -moz-appearance:textfield; }
</style>
    
@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        function loadState() {
            var country_id = $('#country').val();
            if($.trim(country_id) != ''){
                $.get('states', {country_id: country_id}, function(states){

                    var old = $('#state').data('old') != '' ? $('#state').data('old') : '';
                    $('#state').empty();
                    $('#state').append("<option value=''>Selecciona un Estado</option>");
                    $.each(states, function(index, value){
                        $('#state').append("<option value='"+ index +"'" + (old == index ? 'selected' : '') + ">"+ value +"</option>");
                    });
                });
            }
        }
        function loadCity() {
            var state_id = $('#state').val();
            if($.trim(state_id) != ''){
                $.get('cities', {state_id: state_id}, function(cities){

                    var old = $('#city').data('old') != '' ? $('#city').data('old') : '';
                    $('#city').empty();
                    $('#city').append("<option value=''>Selecciona una Ciudad</option>");
                    $.each(cities, function(index, value){
                        $('#city').append("<option value='"+ index +"'" + (old == index ? 'selected' : '') + ">"+ value +"</option>");
                    });
                });
            }
        }
        loadState();
        $('#country').on('change', loadState);
        loadCity();
        $('#state').on('change', loadCity);
    });
</script>
@endsection