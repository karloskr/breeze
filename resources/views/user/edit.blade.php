@extends('layout.home')

@section('title','Edit Users')

@section('content')
@inject('countries', 'App\Services\Countries')   

<div class="container">
    <h3>Edit</h3>
    <form method="POST" action="{{ route('users.update',$user) }}">
        @csrf
        @method('PUT')
        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="id">Id</label>
            <input type="text" class="form-control" id="id" name="id" value="{{ old('name',  $user->id ) }}">
          </div>
          
          <div class="form-group col-md-4">
            <label for="name">Nombre</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name',  $user->name ) }}">
          </div>
          
          <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('name',  $user->email ) }}">
          </div>
         
          <div class="form-group col-md-4">
            <label for="cell">Celular</label>
            <input type="text" class="form-control" id="cell" name="cell" value="{{ old('name',  $user->cell ) }}">
          </div>
          
          <div class="form-group col-md-4">
            <label for="cedula">Cedula</label>
            <input type="text" class="form-control" id="cedula" name="cedula" value="{{ old('name',  $user->cedula ) }}">
          </div>
          
          <div class="form-group col-md-4">
            <label for="nacimiento">Fecha de Nacimiento</label>
            <input type="date" class="form-control" id="nacimiento" name="nacimiento" value="{{ old('name',  $user->nacimiento ) }}">
          </div>
        </div>
      
        <div class="form-row">
          
          <div class="form-group col-md-4">
            <label for="country">Paises</label>
            <select class="form-control" id="country" name="country_id">
              @foreach($countries->get() as $index => $country)
                  <option value="{{ $index }}" {{ old('country_id') == $index ? 'selected' : '' }}>
                      {{ $country }}
                  </option>
              @endforeach
          </select>
          </div>
          
          <div class="form-group col-md-4">
            <label for="state">Estados</label>
            <select class="form-control" id="state" name="state_id" data-old="{{ old('state_id') }}"></select>
          </div>
          
          <div class="form-group col-md-4">
            <label for="city">Ciudades</label>
            <select class="form-control" id="city" name="city_id" data-old="{{ old('city_id') }}"></select>
          </div>
          
        </div>
    
        <button type="submit" class="btn btn-primary">Sign in</button>
      </form>
</div>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        function loadState() {
            var country_id = $('#country').val();
            if($.trim(country_id) != ''){
                $.get('states', {country_id: country_id}, function(states){

                    var old = $('#state').data('old') != '' ? $('#state').data('old') : '';
                    $('#state').empty();
                    $('#state').append("<option value=''>Selecciona un Estado</option>");
                    $.each(states, function(index, value){
                        $('#state').append("<option value='"+ index +"'" + (old == index ? 'selected' : '') + ">"+ value +"</option>");
                    });
                });
            }
        }
        loadState();
        $('#country').on('change', loadState);
    });
</script>
@endsection