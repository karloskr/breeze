@extends('layout.home')

@section('title','Users')

@section('content')
    
<div class="container">
    @auth
    <div class="card-body">
      <h4>Bienvenido - {{ auth()->user()->name }} </h4>
      {{-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search" autocomplete="off">
        <button class="btn btn-outline-success my-2 my-sm-0 mr-4" type="submit">Search</button>
      </form> --}}
   </div>

   <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nombre</th>
        <th scope="col">Email</th>
        <th scope="col">Celular</th>
        <th scope="col">Cedula</th>
        <th scope="col">Edad</th>
        <th scope="col">Ciudad</th>
        <th colspan="2">Acciones</th>
        
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
      <tr>
        <th scope="row">{{ $user->id }}</th>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->cell }}</td>
        <td>{{ $user->cedula }}</td>
        <td>{{ $user->age }}</td>
        <td>{{ $user->city->name }}</td>
        <td>
          <a href="{{ route('users.edit',$user) }}" class="btn btn-success"><i class="fas fa-edit"></i></a>
        </td>
        <td>
          <form action="">
            <a href="" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
            
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
{{ $users->links() }}
    @endauth
</div>
@endsection

@section('css')

<style>
  table tr th {
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.sorting {
  background-color: #D4D4D4;
}

.asc:after {
  content: ' ↑';
}

.desc:after {
  content: " ↓";
}
</style>
    
@endsection

@section('scripts')
    <script>
        $('th').click(function() {
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc) {
      rows = rows.reverse()
    }
    for (var i = 0; i < rows.length; i++) {
      table.append(rows[i])
    }
    setIcon($(this), this.asc);
  })

  function comparer(index) {
    return function(a, b) {
      var valA = getCellValue(a, index),
        valB = getCellValue(b, index)
      return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
    }
  }

  function getCellValue(row, index) {
    return $(row).children('td').eq(index).html()
  }

  function setIcon(element, asc) {
    $("th").each(function(index) {
      $(this).removeClass("sorting");
      $(this).removeClass("asc");
      $(this).removeClass("desc");
    });
    element.addClass("sorting");
    if (asc) element.addClass("asc");
    else element.addClass("desc");
  }
    </script>
@endsection