@extends('layout.home')

@section('title, Contact')
    
@section('content')
    
    <div class="container">
        @include('session-status.message')
        <h1>Contact</h1>
        <form method="POST"  action="{{ route('messages.store') }}">
            @csrf

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" autocomplete="off" value="{{ old('name') }}">
              </div>

                @error('name')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror

            <div class="form-group">
              <label for="email">Email address</label>
              <input type="email" class="form-control" name="email" autocomplete="off" value="{{ old('email') }}">
            </div>

                @error('email')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror

            <div class="form-group">
              <label for="asunto">Asunto</label>
              <input type="text" class="form-control" name="asunto" autocomplete="off" value="{{ old('asunto') }}">
            </div>

                @error('asunto')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror

            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" rows="3">{{ old('content') }}</textarea>
              </div>
              
                @error('content')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror

            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>

@endsection
@endsection