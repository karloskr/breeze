<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subject = 'Mensaje Recibido';

    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->msg = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.message-received');
        /* return $this->from('karlos.kr.2365@gmail.com','Andres')
                        ->subject($this->msg)
                        ->view('emails.message-received')
                        ->with([
                            'name' =>$this->msg,
                            'email' =>$this->msg,
                            'asunto' =>$this->msg,
                            'content' =>$this->msg
                        ]); */
    }
}
