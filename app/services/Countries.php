<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;

class Countries
{
    public function get()
    {
        $countries = Country::get();
        $countriesArray[''] = 'Selecciona un pais';
        foreach ($countries as $country) {
            $countriesArray[$country->id] = $country->name;
        }

        return $countriesArray;
    }

    public function getCity()
    {
        $cities = City::get();
        $citiesArray[''] = 'Selecciona un Ciudad';
        foreach ($cities as $city) {
            $citiesArray[$city->id] = $city->name;
        }

        return $citiesArray;
    }
}