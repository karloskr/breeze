<?php

namespace App\Http\Controllers;

use App\Mail\MessageReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    public function store(Request $request)
    {
        $message = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'asunto' => 'required',
            'content' => 'required',
        ]);

        /* Mail::to('karlos.kr.2365@gmail.com')->send(new MessageReceived($message)); */
        Mail::to('karlos.kr.2365@gmail.com')->send(new MessageReceived($message));

        /* return new MessageReceived($message); *///retornar una instacia de la clase Mailable

        return back()->with('status','Recibimos tu mensaje, te responderemos en menos de 24 horas');
    }
}
