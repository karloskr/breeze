<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {      
        

        $search = $request->get('search');
        
        $users = User::with('city')
        ->union($search)
        ->orderBy('id','desc')
        ->paginate(5);

        return view('user.index',compact('users'));
    }


    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $date = new Carbon();
        $before = $date->subYears(18)->format("d-m-Y");//permite verificar que el usuario registrado sea mayor de edad

        $request->validate([
            'name' => 'required|string|max:100', // longitud 100
            'email' => 'required|string|email|max:255|unique:users', 
            'password' => [
                'required',
                Rules\Password::min(8) //Requiere al menos 8 caracteres
                ->letters()//Requiere al menos una letra
                ->mixedCase()//Requiere al menos una letra mayúscula y una minúscula
                ->numbers()//Requiere al menos un numero
                ->symbols(),//Requiere al menos un caracter/simbolo especial
                'confirmed', Rules\Password::defaults()],
            'cell' => 'nullable|min:10', //campo opcional con una longitud maxima de 10 digitos
            'cedula' => 'required|max:11',
            'nacimiento' => 'required|date|before:'. $before,
            'country_id' => 'required|exists:countries,id',
            'state_id' => 'required|exists:states,id',
            'city_id' => 'required|exists:cities,id',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password), //contraseñas encriptadas
            'cell' => $request->cell,
            'cedula' => $request->cedula,
            'nacimiento' => $request->nacimiento,
            'country_id' => $request->country_id,
            'state_id' => $request->state_id,
            'city_id' => $request->city_id,
        ]);

        event(new Registered($user));

        Auth::login($user);
        return redirect(RouteServiceProvider::HOME);
    }

    public function edit(User $user)
    {
        /* $countries = Country::get();
        $states = State::get();
        $cities = City::get(); */

        return view('user.edit',compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return redirect()->route('users',compact('user'));
    }

    public function getCountries(Request $request)
    {
        if( $request->ajax() ){
            $states = State::where('country_id', $request->country_id)->get();
            foreach ($states as $state) {
                $statesArray[$state->id] = $state->name;
            }
            return response()->json($statesArray);
        }
    }

    public function getCities(Request $request)
    {
        if( $request->ajax() ){
            $cities = City::where('state_id', $request->state_id)->get();
            foreach ($cities as $city) {
                $citiesArray[$city->id] = $city->name;
            }
            return response()->json($citiesArray);
        }
    }

    
}
