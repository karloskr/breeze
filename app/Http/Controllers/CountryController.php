<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = Country::where('name', 'LIKE', "%$query%")->take(10)->get();
      
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
      foreach($data as $row)
      {
       $output .= '
       <li><a href="#" onclick="getElementById(\'country_id\').value = '.$row->id.'">'.$row->name.'</a></li>    
       ';
      }
      $output .= '</ul>';
      echo $output;
     }
    }
}
