<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'cell',
        'cedula',
        'nacimiento',
        'city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['age'];

    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['nacimiento'])->age;//Mutator
    }

    //Query Scopes
    public function scopeName($query, $search)
    {
        if($search)
        return $query->where('name','like', "%$search%");
    }

    public function scopeCedula($query, $search)
    {
        if($search)
        return $query->orWhere('cedula','like', "%$search%");
    }

    public function scopeEmail($query, $search)
    {
        if($search)
        return $query->orWhere('email','like', "%$search%");
    }

    public function scopeCelular($query, $search)
    {
        if($search)
        return $query->orWhere('cell','like', "%$search%");
    }

    public function scopeUnion($query, $search)
    {
        return $query->name($search)
                    ->cedula($search)
                    ->email($search)
                    ->celular($search);
    }
}
