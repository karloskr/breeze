<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $table = 'cities';

    protected $fillable = ['id','name','state_id'];

    public function states()
    {
        return $this->belongsTo(State::class,'state_id');
    }

    public function user()
    {
        return $this->hasMany(User::class,'user_id');
    }
}
