<?php

use App\Http\Controllers\Auth\RegisteredUserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\MessageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

/* Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard'); */

Route::get('/', [HomeController::class,'index'])
/* ->middleware(['auth','verified']) */
->name('home');

Route::view('/about','test.about')->name('about');

Route::resource('portafolio', ProjectController::class)
->parameters(['portafolio' => 'project'])
->names('projects');
/* ->middleware('auth'); */


Route::get('/users', [RegisteredUserController::class,'index'])
->middleware(['auth'])
->name('users.index');
Route::get('/users/{user}/edit',[RegisteredUserController::class,'edit'])->name('users.edit');
Route::put('/users/{user}',[RegisteredUserController::class,'update'])->name('users.update');


Route::get('/states', [RegisteredUserController::class,'getCountries']);
Route::get('/cities', [RegisteredUserController::class,'getCities']);

Route::view('/contact','test.contact')->name('contact');
Route::post('/contact', [MessageController::class,'store'])->name('messages.store');


require __DIR__.'/auth.php';
